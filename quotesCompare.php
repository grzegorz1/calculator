<?php
/**
 * Created by PhpStorm.
 * User: grzegorz
 * Date: 09.03.2016
 * Time: 15:02
 */

require('BankQuotes.php');
require('PipserQuotes.php');

class QuotesCompare
{

    private function processUrlPipser($currency)
    {
        $time = new DateTime();
        $now = $time->format(DATE_ISO8601);
        $interval = (new DateInterval('PT1H'));
        $end = $time->sub($interval)->format(DATE_ISO8601);
        $startDate = '&startDate=' . urlencode($end);
        $endDate = '&endDate=' . urlencode($now);
        $urlPipser = "https://d3ev96jse7ksor.cloudfront.net/quote?sort=DESC&client=f5b6eb18-5303-4f58-bd3c-040d8d949077&page_size=1&currency=" . $currency . $startDate . $endDate;
        return $urlPipser;
    }

    private function processUrlBank($currency)
    {
        return "http://spred.pl/waluta/" . $currency;
    }
    private function getIndex($ofWhat,$pipserQuotes)
    {
        $counter = 0;
        foreach ($pipserQuotes as $i) {

            if ($i["currency"] == $ofWhat) {
                break;
            }
            $counter++;
        }
            return $counter;
    }
    private function compareQuotes($currency)
    {
        //$urlPipser = "https://d3ev96jse7ksor.cloudfront.net/quote?sort=DESC&client=f5b6eb18-5303-4f58-bd3c-040d8d949077&page_size=1&currency=EUR&startDate=2016-03-09T10%3A16%3A35-0100&endDate=2016-03-09T12%3A16%3A35-0100";
        $urlPipser = $this->processUrlPipser($currency);
        $urlBank = $this->processUrlBank($currency);
        $bank = new BankQuotes($urlBank);
        $pipser = new PipserQuotes($urlPipser);

        $banksQ = $bank->getBankQuates();
        $pipserQ = $pipser->getPipserQuotes();

        $realPrices = [];
        $counter = $this->getIndex('PLN', $pipser);
        foreach ($banksQ as $single) {
            $s = array();
            $s["title"] = $single["title"];
            $s["saving"] = ((float)$single["sell"] - (float)$pipserQ[$counter]["sell"]);
            if (((float)$s["saving"]) < 0) {
                $s["saving"] = (float)$s["saving"] * (-1);
            }
            array_push($realPrices, $s);
        }
        return $realPrices;
    }

    public function saveJson($pathToFile)
    {
        $currencyArray = ['CHF', 'GBP', 'EUR', 'USD'];
        $jsons = array();
        foreach ($currencyArray as $currency) {
            $line["currency"] = $currency;
            $line["savings"] = $this -> compareQuotes($currency);
            array_push($jsons, $line);
        }
        $json = json_encode($jsons);
        file_put_contents($pathToFile, $json);
    }
}
$path = "savings.json";
$temp = new QuotesCompare();
$temp->saveJson($path);

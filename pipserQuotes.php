<?php
/**
 * Created by PhpStorm.
 * User: grzegorz
 * Date: 09.03.2016
 * Time: 13:10
 */
class PipserQuotes
{
    private $url;
    private $quotes;

    /**
     * pipserQuates constructor.
     */
    private function download_remote_file_with_curl($file_url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch,CURLOPT_URL,$file_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $file_content = curl_exec($ch);
        curl_close($ch);
        return $file_content;
    }

    public function processQuotes($url)
    {
        $this ->changeUrl($url);
        $json = $this->download_remote_file_with_curl($url);

        $obj = json_decode($json);

        $priceList = $obj->_embedded->quote[0]->prices;
        $currencyList = array_keys((array)$priceList);

        $pipserQuotes = [];
        foreach($currencyList as $currency)
        {
            $newElement=array();
            $newElement["currency"]=$currency;
            $newElement["sell"]=$priceList->$currency->sell;
            array_push($pipserQuotes,$newElement);
        }

        return $pipserQuotes;
    }

    public function __construct($file_url)
    {
        $this->changeUrl($file_url);
        $this->changeQuotes($this->processQuotes($this->url));
    }

    /**
     * @param mixed $url
     */
    public function changeUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * @return array
     */
    public function getPipserQuotes()
    {
        return $this->quotes;
    }

    /**
     * @param array $quotes
     */
    public function changeQuotes($quotes)
    {
        $this->quotes = $quotes;
    }

}
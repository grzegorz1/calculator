<?php
/**
 * Created by PhpStorm.
 * User: grzegorz
 * Date: 04.03.2016
 * Time: 13:04
 */
require ('simple_html_dom.php');
class BankQuotes
{
    private $url;
    private $bankQuates;

    private function getRows($url)
    {
        $html = file_get_html($url);
        $table = $html->find('table');
        $tr = $table[0]->find('tr');
        return $tr;
    }

    public function processQuotes($url)
    {
        $this->changeUrl($url);
        $tr = $this->getRows($url);
        $bankQuotes = [];

        foreach ($tr as $i) {
            $newElement = array();
            $col = $i->find('td');

            if (count($col) > 0) {
                $newElement["title"] = $col[0]->plaintext;
                $newElement["currency"] = $col[1]->plaintext;
                $newElement["sell"] = $col[3]->plaintext;
                array_push($bankQuotes, $newElement);
            }
        }

        return $bankQuotes;
    }
    /**
     * BankQuotes constructor.
     */
    public function __construct($url)
    {
        $this->changeUrl($url);
        $this->setBankQuates($this->processQuotes($url));
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function changeUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getBankQuates()
    {
        return $this->bankQuates;
    }

    /**
     * @param mixed $bankQuates
     */
    public function setBankQuates($bankQuates)
    {
        $this->bankQuates = $bankQuates;
    }

}